import mongoose from 'mongoose'

export function cleanDb () {
  for (const collection in mongoose.connection.collections) {
    if (mongoose.connection.collections.hasOwnProperty(collection)) {
      mongoose.connection.collections[collection].remove()
    }
  }
}

export function authUser (agent, callback) {
  agent
    .post('/users/register')
    .set('Accept', 'application/json')
    .send({user:{username: 'test',password:'Password1234',email:"test@test.fr",age:13,gender:'male'}})
    .end((err, res) => {
      if (err) { return callback(err) }

      callback(null, {
        user: res.body.user,
        token: res.body.token
      })
    })
}
