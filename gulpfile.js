'use strict'

const Gulp = require('gulp')
const RequireDir = require('require-dir')

// Load tasks
RequireDir('./tasks');

Gulp.task(
  'dev',
  Gulp.series( 'dotenv', 'nodemon')
);

Gulp.task(
  'prod',
  Gulp.series('pm2')
);

Gulp.task(
  'default',
  Gulp.series('dev')
);
