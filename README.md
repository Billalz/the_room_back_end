## The Room Back End

## Description
This project is back end for application The Room

Visit `https://localhost:3000/` to access the root page.

## Requirements
* node > v8.9.4
* MongoDB

## Installation
```
git clone git@gitlab.com:Billalz/the_room_back_end.git
npm install
```

## Features
* Login
* Register
* Chat with WebSocket
* Secure session with token

## Usage
* `gulp prod` Start server on production 
* `gulp` Start server on development
* `npm run docs` Generate API documentation
* `npm test` Run mocha tests

