export function getToken (ctx) {
  const header = ctx.request.header.authorization
  if (!header) {
    return null
  }
  const parts = header.split(' ')
  if (parts.length !== 2) {
    return null
  }
  const scheme = parts[0]
  const token = parts[1]
  if (/^Bearer$/i.test(scheme)) {
    return token
  }
  return null
}

export function getError(ctx, code, message) {
  let error = {};
  error.code = code;
  error.message = message;
  ctx.status = code;
  ctx.body = error;
  return ctx;
}
