import { getError } from "../utils/auth";

export function errorMiddleware () {
  return async (ctx, next) => {
    try {
      await next()
    } catch (err) {
      ctx.status = err.status || 500
      ctx = getError(ctx, err.status || 500, err.message);
      ctx.app.emit('error', err, ctx)
    }
  }
}
