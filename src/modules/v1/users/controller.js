import User from '../../../models/users'
import { verify } from 'jsonwebtoken'
import config from '../../../../config'

import { getError } from "../../../utils/auth";

/**
 * @api {post} /v1/users Create a new user
 * @apiPermission
 * @apiVersion 1.0.0
 * @apiName CreateUser
 * @apiGroup Users
 *
 * @apiExample Example usage:
 * curl -H "Content-Type: application/json" -X POST -d '{ "user": { "username": "johndoe", "password": "secretpasas" } }' localhost:3000/v1/users
 *
 * @apiParam {Object} user          User object (required)
 * @apiParam {String} user.username Username.
 * @apiParam {String} user.password Password.
 *
 * @apiSuccess {Object}   users           User object
 * @apiSuccess {ObjectId} users._id       User id
 * @apiSuccess {String}   users.name      User name
 * @apiSuccess {String}   users.username  User username
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "user": {
 *          "_id": "56bd1da600a526986cf65c80"
 *          "name": "John Doe"
 *          "username": "johndoe"
 *       }
 *     }
 *
 * @apiError UnprocessableEntity Missing required parameters
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 422 Unprocessable Entity
 *     {
 *       "status": 422,
 *       "error": "Unprocessable Entity"
 *     }
 */
export async function createUser (ctx) {
  const user = new User(ctx.request.body.user);
  try {
    await user.save()
  } catch (err) {
    if (err.code === 11000) {
      let error = {};
      if (err.message.includes('email')){
        error.code = 409;
        error.email = {};
        error.email.message = 'An account with this email address already exists';
        error.errors = err.errors;
        ctx.body = error;
        ctx.status = 409;
      }else if (err.message.includes('username')){
        error.code = 409;
        error.username = {};
        error.username.message = 'This username already exists, choose other username';
        error.errors = err.errors;
        ctx.body = error;
        ctx.status = 409;
      }
     return
    }
  else{
      if (err) {
        let error = {};
        error.code = 400;
        error.message = 'User validation failed';
        error.errors = err.errors;
        ctx.body = error;
        ctx.status = 400;
        return
      }
    }
  }



  const token = user.generateToken();
  const response = user.toJSON();

  delete response.password;

  ctx.body = {
    code: 200,
    token,
    user: response,
  }
}








export async function validateToken (ctx) {
  if ('token' in ctx.request.body){
    try {
      verify(ctx.request.body.token, config.token)
    }catch (e) {
     ctx = getError(ctx,401,'Unauthorized');
      return
    }
  }else {
    ctx = getError(ctx,400,'token is required');
    return
  }
  ctx.body = {
    code: 200,
    message: 'Token is valid'
  }
}



/**
 * @api {get} /v1/users Get all users
 * @apiPermission user
 * @apiVersion 1.0.0
 * @apiName GetUsers
 * @apiGroup Users
 *
 * @apiExample Example usage:
 * curl -H "Content-Type: application/json" -X GET localhost:3000/v1/users
 *
 * @apiSuccess {Object[]} users           Array of user objects
 * @apiSuccess {ObjectId} users._id       User id
 * @apiSuccess {String}   users.name      User name
 * @apiSuccess {String}   users.username  User username
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "users": [{
 *          "_id": "56bd1da600a526986cf65c80"
 *          "name": "John Doe"
 *          "username": "johndoe"
 *       }]
 *     }
 *
 * @apiUse TokenError
 */
export async function getUsers (ctx) {
  const users = await User.find({}, '-password');
  ctx.body = {
    code:200,
    users
  }
}

/**
 * @api {get} /v1/users/:id Get user by id
 * @apiPermission user
 * @apiVersion 1.0.0
 * @apiName GetUser
 * @apiGroup Users
 *
 * @apiExample Example usage:
 * curl -H "Content-Type: application/json" -X GET localhost:3000/v1/users/56bd1da600a526986cf65c80
 *
 * @apiSuccess {Object}   users           User object
 * @apiSuccess {ObjectId} users._id       User id
 * @apiSuccess {String}   users.name      User name
 * @apiSuccess {String}   users.username  User username
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "user": {
 *          "_id": "56bd1da600a526986cf65c80"
 *          "name": "John Doe"
 *          "username": "johndoe"
 *       }
 *     }
 *
 * @apiUse TokenError
 */
export async function getUser (ctx, next) {
  try {
    const user = await User.findById(ctx.params.id, '-password');
    if (!user) {
      ctx.throw(404)
    }

    ctx.body = {
      code:200,
      user
    }
  } catch (err) {
    let error = {};
    if (err === 404 || err.name === 'CastError') {
      error.code = 404;
      error.message = 'User not found';
      ctx.status = 404;
      ctx.body = error;
      return
    }
    ctx.throw(500)
  }

  if (next) { return next() }
}

/**
 * @api {put} /v1/users/:id Update a user
 * @apiPermission
 * @apiVersion 1.0.0
 * @apiName UpdateUser
 * @apiGroup Users
 *
 * @apiExample Example usage:
 * curl -H "Content-Type: application/json" -X PUT -d '{ "user": { "name": "Cool new Name" } }' localhost:3000/v1/users/56bd1da600a526986cf65c80
 *
 * @apiParam {Object} user          User object (required)
 * @apiParam {String} user.name     Name.
 * @apiParam {String} user.username Username.
 *
 * @apiSuccess {Object}   users           User object
 * @apiSuccess {ObjectId} users._id       User id
 * @apiSuccess {String}   users.name      Updated name
 * @apiSuccess {String}   users.username  Updated username
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "user": {
 *          "_id": "56bd1da600a526986cf65c80"
 *          "name": "Cool new name"
 *          "username": "johndoe"
 *       }
 *     }
 *
 * @apiError UnprocessableEntity Missing required parameters
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 422 Unprocessable Entity
 *     {
 *       "status": 422,
 *       "error": "Unprocessable Entity"
 *     }
 *
 * @apiUse TokenError
 */
export async function updateUser (ctx) {
  const user = ctx.body.user;
  Object.assign(user, ctx.request.body.user);

  await user.save();

  ctx.status = 200;
  ctx.body = {
    code: 200,
    user
  }
}

/**
 * @api {delete} /v1/users/:id Delete a user
 * @apiPermission
 * @apiVersion 1.0.0
 * @apiName DeleteUser
 * @apiGroup Users
 *
 * @apiExample Example usage:
 * curl -H "Content-Type: application/json" -X DELETE localhost:3000/v1/users/56bd1da600a526986cf65c80
 *
 * @apiSuccess {StatusCode} 200
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true
 *     }
 *
 * @apiUse TokenError
 */

export async function deleteUser (ctx) {
  const user = ctx.body.user;

  await user.remove();

  ctx.status = 200;
  ctx.body = {
    success: true
  }
}
