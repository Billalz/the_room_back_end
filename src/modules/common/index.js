import glob from 'glob'
import Router from 'koa-router'
const Cors = require('@koa/cors');


exports = module.exports = function initModules (app) {
  glob(`${__dirname}/*`, { ignore: '**/index.js' }, (err, matches) => {
    if (err) { throw err }

    const koaOptions = {
      origin: '*',
      credentials: true,
    };

    matches.forEach((mod) => {
      console.log(`${mod}/router`)
      const router = require(`${mod}/router`)
      const routes = router.default
      const baseUrl = router.baseUrl
      const instance = new Router({ prefix: baseUrl })
      routes.forEach((config) => {
        const {

          method = '',
          route = '',
          handlers = []
        } = config

        const lastHandler = handlers.pop()

        instance[method.toLowerCase()](route, ...handlers, async function (ctx) {
          await lastHandler(ctx)
        })

        app
          .use(instance.routes())
          .use(instance.allowedMethods()).use(Cors(koaOptions))
      })
    })
  })
}
