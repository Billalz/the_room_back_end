import * as auth from './controller'

export const baseUrl = '/login'

export default [
  {
    method: 'POST',
    route: '/',
    handlers: [
      auth.authUser
    ]
  }
]
