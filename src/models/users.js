import mongoose from 'mongoose'
import bcrypt from 'bcrypt'
import config from '../../config'
import jwt from 'jsonwebtoken'
require('mongoose-type-email');

const User = new mongoose.Schema({
  role: { type: String, default: 'User' },
  username: { type: String, required:  [true, 'Username is required.'] , unique: true },
  email: {
    type: mongoose.SchemaTypes.Email,
    required: [true, 'Email is required.'],
    index: {
      unique: [true, 'Hello'],
      sparse: true
    }
  },
  password: { type: String, required: [true, 'Password is required.'], validate:[validatePassword, 'The password must contain at least 8 characters with have at least one uppercase and one lowercase letter and one number'] },
  age: {type: Number,required: [true,'Age is required'] ,min: [13,'The minimum age required is 13 years old']},
  gender: {type: String , required:[true,'Gender is Required'], validate:[validateGender, "Invalid gender"]},
  isConnected: {
    type: Boolean,
    default: false,
    required: true
  }
});

function validatePassword(value) {
  return /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/.test(value);
}

function validateGender(value) {
  return !!(value === 'male' || value === 'female');
}

User.pre('save', function preSave (next) {
  const user = this;

  if (!user.isModified('password')) {
    return next()
  }

  new Promise((resolve, reject) => {
    bcrypt.genSalt(10, (err, salt) => {
      if (err) { return reject(err) }
      resolve(salt)
    })
  })
    .then(salt => {
      bcrypt.hash(user.password, salt, (err, hash) => {
        if (err) { throw new Error(err) }

        user.password = hash;

        next(null)
      })
    })
    .catch(err => next(err))
});

User.methods.validatePassword = function validatePassword (password) {
  const user = this

  return new Promise((resolve, reject) => {
    bcrypt.compare(password, user.password, (err, isMatch) => {
      if (err) { return reject(err) }

      resolve(isMatch)
    })
  })
};

User.methods.generateToken = function generateToken () {
  const user = this;

  return jwt.sign({ id: user.id }, config.token)
};

export default mongoose.model('user', User)
